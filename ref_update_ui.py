'''
Created on 18 ene. 2020

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool folder is

# define env var for refernce path
import os
os.environ['EXAMPLE_PATH'] = os.path.join(path, 'ref_update')

# open scene
from maya import cmds
ref_scene = os.path.join(path, 'ref_update', 'refScene.ma')
cmds.file(ref_scene, o=1, f=1)

# import and run script
import sys
sys.path.append(path)

from ref_update import ref_update_ui
reload(ref_update_ui)
ui = ref_update_ui.MainUI()
ui.show()

# update all without ui
ref_update_ui.MainUI().updateAll()

@author: eduardo
'''
import os
import re
import glob
import functools
import datetime
import string

from maya import cmds
from PySide2 import QtWidgets

import loadUiType


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'main.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)

DEFAULT_REGEX_LIST = ['v[0-9]{3}', '_[0-9]{3}']


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, regex_list=None, root_only=True):

        # get maya main wndows as a qwidget so we can parent our ui
        parent = getMayaMainWindow()

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        self.root_ref_checkBox.setChecked(root_only)

        if regex_list is None:
            regex_list = DEFAULT_REGEX_LIST
        self.setRegexList(regex_list)

        # overwriting popup menu fo list widget
        self.listWidget.contextMenuEvent = self.customContextMenuEvent

        # connect all buttons
        self.sel_pushButton.clicked.connect(self.updateSelected)
        self.all_pushButton.clicked.connect(self.updateAll)
        self.outdated_pushButton.clicked.connect(self.selectOudatedItems)
        self.refresh_pushButton.clicked.connect(self.refresh)
        self.root_ref_checkBox.toggled.connect(self.refresh)

        # get reference data and load it the list widget
        self.refresh()

        # select items that are outdated
        self.selectOudatedItems()

    def setRegexList(self, regex_list):
        ''' Sets the regex in the ui
        Args:
            regex_list (list[str]): list of regex to use to find versions in order
        Returns:
            None
        '''
        self.regex_lineEdit.setText('')
        if not regex_list:
            return
        line = ','.join(regex_list)
        self.regex_lineEdit.setText(line)

    def getRegexList(self):
        ''' Returns the regex in the ui. If ui has not regex, it returns the default ones
        Args:
            None
        Returns:
            regex_list (list[str]): list of regex to use to find versions in order
        '''
        line = self.regex_lineEdit.text()

        if line:
            regex_list = line.split(',')
        else:
            regex_list = DEFAULT_REGEX_LIST

        return regex_list

    def refresh(self):
        ''' gets reference data from current scene, and populates the list widget'''

        # clear the list from previous content
        self.listWidget.clear()

        # get reference data
        only_root = self.root_ref_checkBox.isChecked()
        ref_data = getReferecesData(only_root=only_root)
        if not ref_data:
            return

        # add each reference to the list
        ref_data.sort(key=lambda d: d['namespace'])
        for ref in ref_data:

            # build item text
            prefix = '+-' if ref['child'] else ''
            name = '{}{}: {}'.format(prefix, ref['namespace'], ref['path'])

            # create the item, set the text and data
            item = QtWidgets.QListWidgetItem(self.listWidget)
            item.setText(name)
            item.ref_data = ref

            # add item to the list
            self.listWidget.addItem(item)

    def selectOudatedItems(self):
        ''' Select items representing the references that are outdated '''

        # loop through all the items
        all_items = self.getAllItems()
        for item in all_items:

            # get the path from the data in the item and using PathHandler find out if its the latest
            item_data = self._itemToData(item)
            is_latest = PathHandler(item_data['path'], regex_list=self.getRegexList()).isLatest()

            # set the item selected if it's not the latest
            item.setSelected(not is_latest)

    def getSelectedItems(self):
        ''' returns the items selected in the list widget as list of QListWidgetItem '''
        selected_items = self.listWidget.selectedItems()
        return selected_items

    def getSelectedRefs(self):
        ''' returns the items selected in the list widget as a list of dictionaries with ref data '''
        selected_items = self.getSelectedItems()
        return self._itemsToData(selected_items)

    def getAllItems(self):
        ''' returns all the items in the list widget as a list of dictionaries with ref data '''
        all_items = [self.listWidget.item(row) for row in range(self.listWidget.count())]
        return all_items

    def getAllRefs(self):
        ''' returns all the items in the list widget as a list of dictionaries with ref data '''
        all_items = self.getAllItems()
        return self._itemsToData(all_items)

    def _itemToData(self, item):
        ''' extracts the reference data from one item and return it as a dict '''
        return item.ref_data

    def _itemsToData(self, item_list):
        ''' extracts the reference data from the items and return it as a list of dict'''
        result = []

        # loop through each item and extract the data stored in it
        for item in item_list:
            result.append(self._itemToData(item))

        return result

    def updateAll(self):
        ''' callback for updating all references to latest version found '''

        data_list = self.getAllRefs()
        self._updateToLatest(data_list)
        self.refresh()

    def updateSelected(self):
        ''' callback for updating selected references to latest version found '''

        data_list = self.getSelectedRefs()
        self._updateToLatest(data_list)
        self.refresh()

    def _updateToLatest(self, data_list):
        ''' utitlity method to update a list of reference data to the latest version '''

        for data in data_list:

            # get the reference path and with the PathHandler class get the lates file
            orig_path = data['path']
            ph = PathHandler(orig_path, regex_list=self.getRegexList())
            latest = ph.getLatest()

            # check that we found some path, if no path found, move to the next reference
            if not latest:
                print 'Cant find latest for {}'.format(orig_path)
                continue

            # now get the reference and replace the path to the lates
            ref = data['ref']
            self._replaceRef(ref, latest)

    def customContextMenuEvent(self, event):
        ''' Context menu to switch path linked to the version '''

        # create menu
        menu = QtWidgets.QMenu(self)

        # look for current item in the list widget
        current_item = self.listWidget.currentItem()
        if not current_item:
            return

        # leave only one item selected (in case ther are many) so user knows what
        # it's neeing edited
        selected_items = self.getSelectedItems()
        for item in selected_items:
            item.setSelected(id(item) == id(current_item))

        # this will process the redraw request generated when the selection was changed
        # otherwise the selection will change but the user will not see it
        QtWidgets.QApplication.processEvents()

        # get the current's item data
        item_data = self._itemToData(current_item)

        # get the reference to modify from the data
        ref = item_data['ref']

        # get the path from data and with the PathHandler class list all the versions found
        orig_path = item_data['path']
        ph = PathHandler(orig_path, regex_list=self.getRegexList())
        path_list = ph.getVersions()

        # add each path as a item in the menu
        for path in path_list:

            # construct the item text with the path and the path modification time
            # a new PathHandler instance helps getting the mtime
            stamp = PathHandler(path).getMTimeStr('%d/%m/%Y %H:%M')
            text = '{} ({})'.format(path, stamp)

            # constructs the action
            action = QtWidgets.QAction(text, menu)
            # add the command. Note the flag refresh=True so after changing the path, the ui refreshes
            cmd = functools.partial(self._replaceRef, ref, path, refresh=True)
            action.triggered.connect(cmd)

            # if the path is the same we already have, make the item in the menu disabled
            action.setEnabled(not ph.isSamePath(path))

            # add the action to the menu
            menu.addAction(action)

        # show the menu and wait for user interaction
        menu.exec_(event.globalPos())

    def _replaceRef(self, ref, path, refresh=False):
        ''' Replace in a reference the path '''
        print 'replacing {} with {}'.format(ref, path)

        # loads the path into the reference
        cmds.file(path, lr=ref)

        # if refresh is True, lets refresh the whole ui
        if refresh:
            self.refresh()


class PathHandler(object):
    ''' class to handle the path listing and other file system related methods '''

    # regex for finding the version, for example v004
    default_version_str = 'v[0-9]{3}'

    def __init__(self, path, regex_list=None):
        ''' inits the class with a full path
        Args:
            path (str): full path to the file to handle
            regex_list (list[str]): the list of regex to try in order to find the version number
        '''
        self.original_path = path

        if regex_list is None:
            regex_list = [self.default_version_str]

        self.regex_list = regex_list

    def _getGlob(self):
        ''' gets a glob path from the original path by replacing the version numbers with ? '''

        # find all occurences of a version token
        # attempt wiht multiple tokens
        all_versions = []
        for regex in self.regex_list:
            all_versions = re.findall(regex, self.original_path)
            if all_versions:
                break

        # start with original path and replace each ocurrence
        glob_path = self.original_path
        for version in all_versions:

            # construct the glob like part by replacing the numbers with ?
            version_glob = ''.join(['?' if v in string.digits else v for v in version])

            # replace the version wiht the partial glob
            glob_path = glob_path.replace(version, version_glob)

        return glob_path

    def getVersions(self):
        ''' list all versions of the original path using glob '''

        # list versions with glob
        glob_path = self._getGlob()
        path_list = glob.glob(glob_path)

        # make sure paths are all in the same style
        path_list = [cleanPath(p) for p in path_list[:]]

        # order by version
        path_list.sort()

        return path_list

    def getLatest(self):
        ''' returns the latest path by listing all and returning the last one '''

        # get all versions
        version_list = self.getVersions()

        # check we found at leat one path
        if not version_list:
            return

        # return the last one. Since they are in order, it will be the last version
        return version_list[-1]

    def isLatest(self):
        ''' Returns whether or not the original path is the latest one '''

        # get the latest path
        latest = self.getLatest()

        # check we got something
        if not latest:
            return False

        # if latest path is the same as the original path, then original path is the latest
        return self.isSamePath(latest)

    def isSamePath(self, other_path):
        ''' returns True if the other path is the same as the original path. This is not so trivial in windows
        since there are many ways of writting the same path '''

        # compare to lower and with the same bars
        clean_orig = cleanPath(self.original_path).lower()
        clean_other = cleanPath(other_path).lower()

        return clean_orig == clean_other

    def getMTime(self):
        ''' Returns the modification time of original path '''
        mtime = os.path.getmtime(self.original_path)
        date = datetime.datetime.fromtimestamp(mtime)
        return date

    def getMTimeStr(self, format_str):
        ''' Returns the modification time of original path as a formatted string '''
        return self.getMTime().strftime(format_str)


def getReferecesData(only_root=False):
    ''' Returns data from the references found in current maya scene
    Args:
        only_root (bool): whether or not to ignore childe reference
    Returns:
        list[dict]: a list of dictionaries each representing a reference with the following keys:
            ref (str): name of the refenrence node
            path (str): path loaded on the reference now
            namespace (str): the namespace of the reference
    '''

    result = []

    # list all references in maya
    references = cmds.ls(type='reference')
    for r in references:

        # get path and namespace from each reference found
        try:

            # skip child reference if requested
            is_sub_ref = cmds.referenceQuery(r, isNodeReferenced=True)

            if only_root and is_sub_ref:
                continue
                 
            path = cmds.referenceQuery(r, f=1, wcn=True)
            path = cleanPath(path)
            namespace = cmds.referenceQuery(r, ns=True)
        except Exception, e:  # @UnusedVariable
            print 'cant get data form reference {}'.format(r)
            continue

        # create a dictionary with the data and add it to the resutl list
        data = {'ref': r, 'path': path, 'namespace': namespace, 'child': is_sub_ref}
        result.append(data)

    return result


def cleanPath(path):
    ''' normalises a path so it does not have duplicated bars or different kind of bars '''
    return os.path.normpath(path).replace('\\', '/')


def getMayaMainWindow():
    ''' Tries to get the maya main window
    Args:
        None
    Returns:
        QWidget or None: the maya main window or None if it could not be found
    '''

    try:
        # we need to wrap a pointer as a pyside widget so we need help!
        from shiboken2 import wrapInstance
        from maya import OpenMayaUI as omui

        # get opinter object
        ptr = omui.MQtUtil.mainWindow()

        # cast pointer to widget (may fail)
        main_window = wrapInstance(long(ptr), QtWidgets.QWidget)

    except Exception, e:  # @UnusedVariable
        main_window = None

    return main_window
